package provider.mappers.database;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import provider.domain.Role;
import provider.domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class UserMapperTest {

    private UserMapper mapper = new UserMapper();

    @Test
    public void map() throws Exception {
        final User expectedUser = new User(1, "a", "a", "a", "a", Role.USER, 1.0, false);

        ResultSet resultSet = prepareResultSet();

        final User actualUser = mapper.map(resultSet);
        Assert.assertEquals(expectedUser, actualUser);
    }

    private ResultSet prepareResultSet() throws SQLException {

        final ResultSet mock = Mockito.mock(ResultSet.class);
        when(mock.getInt("id")).thenReturn(1);
        when(mock.getString("email")).thenReturn("a");
        when(mock.getString("password")).thenReturn("a");
        when(mock.getString("name")).thenReturn("a");
        when(mock.getString("surname")).thenReturn("a");
        when(mock.getString("role")).thenReturn("USER");
        when(mock.getDouble("balance")).thenReturn(1.0);
        when(mock.getBoolean("isBlocked")).thenReturn(false);

        return mock;
    }

}