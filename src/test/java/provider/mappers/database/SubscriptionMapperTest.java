package provider.mappers.database;

import com.mysql.cj.jdbc.result.ResultSetImpl;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import provider.domain.Subscription;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class SubscriptionMapperTest {

    private SubscriptionMapper mapper = new SubscriptionMapper();

    @Test
    public void map() throws Exception {

        final Date date = new Date();
        final Subscription expectedSubscription = new Subscription(1, 1, 1, date, date);
        ResultSet resultSet = prepareResultSet(date);
        final Subscription actualSubscription = mapper.map(resultSet);
        Assert.assertEquals(expectedSubscription, actualSubscription);
    }

    private ResultSet prepareResultSet(Date date) throws SQLException {
        final ResultSet mock = Mockito.mock(ResultSet.class);
        when(mock.getInt("id")).thenReturn(1);
        when(mock.getInt("user_id")).thenReturn(1);
        when(mock.getInt("tariff_id")).thenReturn(1);
        when(mock.getDate("startDate")).thenReturn(new java.sql.Date(date.getTime()));
        when(mock.getDate("endDate")).thenReturn(new java.sql.Date(date.getTime()));

        return mock;
    }

}