package provider.mappers.database;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import provider.domain.Tariff;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class TariffMapperTest {

    private TariffMapper mapper = new TariffMapper();

    @Test
    public void map() throws Exception {
        final Tariff expectedTariff = new Tariff(1, "a", 1.0, 1.0);

        ResultSet resultSet = prepareResultSet();
        final Tariff actualTariff = mapper.map(resultSet);

        Assert.assertEquals(expectedTariff, actualTariff);
    }

    private ResultSet prepareResultSet() throws SQLException {

        final ResultSet mock = Mockito.mock(ResultSet.class);
        when(mock.getInt("id")).thenReturn(1);
        when(mock.getString("name")).thenReturn("a");
        when(mock.getDouble("price")).thenReturn(1.0);
        when(mock.getDouble("speed")).thenReturn(1.0);

        return mock;
    }

}