package provider.mappers.request;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import provider.domain.User;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class UserMapperTest {

    private UserMapper mapper = new UserMapper();

    @Test
    public void map() throws Exception {
        final User expected = new User("aaa@aaa.com", "a");

        HttpServletRequest request = prepareRequest();
        final User actual = mapper.map(request);
        Assert.assertEquals(expected, actual);
    }

    private HttpServletRequest prepareRequest() {

        final HttpServletRequest mock = Mockito.mock(HttpServletRequest.class);
        when(mock.getParameter("name")).thenReturn("a");
        when(mock.getParameter("surname")).thenReturn("a");
        when(mock.getParameter("password")).thenReturn("a");
        when(mock.getParameter("email")).thenReturn("aaa@aaa.com");

        return mock;
    }

}