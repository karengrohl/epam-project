package provider.mappers.request;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import provider.domain.Transaction;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransactionMapperTest {

    private TransactionMapper mapper = new TransactionMapper();

    @Test
    public void map() throws Exception {
        final Transaction expected = new Transaction(1.0);
        HttpServletRequest request = prepareRequest();
        final Transaction actual = mapper.map(request);
        Assert.assertEquals(expected, actual);
    }

    private HttpServletRequest prepareRequest() {
        final HttpServletRequest mock = Mockito.mock(HttpServletRequest.class);
        when(mock.getParameter("amount")).thenReturn("1.0");
        return mock;
    }

}