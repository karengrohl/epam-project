package provider.mappers.request;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import provider.domain.Tariff;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class TariffMapperTest {

    private TariffMapper mapper = new TariffMapper();

    @Test
    public void map() throws Exception {
        final Tariff expectedTariff = new Tariff("abc", 1.0, 1.0);

        HttpServletRequest request = prepareRequest();
        final Tariff actualTariff = mapper.map(request);
        Assert.assertEquals(expectedTariff, actualTariff);

    }

    private HttpServletRequest prepareRequest() {
        final HttpServletRequest mock = Mockito.mock(HttpServletRequest.class);
        when(mock.getParameter("name")).thenReturn("abc");
        when(mock.getParameter("price")).thenReturn("1.0");
        when(mock.getParameter("speed")).thenReturn("1.0");

        return mock;
    }

}