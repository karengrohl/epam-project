package provider.validation;

import org.junit.Test;
import provider.domain.User;

import static org.junit.Assert.*;

public class UserValidatorTest {

    private Validator<User> validator = new UserValidator();

    @Test(expected = ValidationException.class)
    public void validateEmail() throws Exception {
        final User user = new User("abc", "abc");
        validator.validate(user);
    }

    @Test(expected = ValidationException.class)
    public void validateName() throws Exception {
        final User user = new User();
        user.setEmail("user@email.com");
        user.setName(produceString(55));
        validator.validate(user);
    }

    private String produceString(int length) {

        StringBuilder builder = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            builder.append("a");
        }

        return builder.toString();
    }
}