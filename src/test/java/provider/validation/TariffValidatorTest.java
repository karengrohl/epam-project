package provider.validation;

import org.junit.Before;
import org.junit.Test;
import provider.domain.Tariff;

import static org.junit.Assert.*;

public class TariffValidatorTest {

    private Validator<Tariff> validator = new TariffValidator();

    @Test(expected = ValidationException.class)
    public void validate() throws Exception {
        final Tariff tariff = new Tariff();
        tariff.setName("A");
        validator.validate(tariff);
    }

}