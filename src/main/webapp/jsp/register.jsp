<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Registration</title>
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<h1>Регистрация</h1>
<form action="/controller/register" method="post">
    <div class="container">
        <p>Please fill in this form to create an account.</p>
        <hr>

        <label><b>Email</b>
        <input type="email" placeholder="Enter Email" required name="email">
        </label>

        <label><b>Password</b>
        <input type="password" placeholder="Enter Password" required name="password">
        </label>

        <hr>

        <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>
    <input class="button" type="submit" value="Зарегистрироваться">
    </div>
</form>
<c:import url="components/footer.jsp"/>
</body>
</html>
