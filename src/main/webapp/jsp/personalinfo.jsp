<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="localizations" var="rb"/>
<html>
<head>
    <title><fmt:message key="path.page.personalinfo.title" bundle="${rb}"/></title>
</head>
<c:import url="components/header.jsp"></c:import>
<body>
<h1><fmt:message key="path.page.personalinfo.title" bundle="${rb}"/></h1>
<form action="/controller/personalinfo/save" method="post" class="flex-container">
    <label>
        <fmt:message key="page.admin.label.name" bundle="${rb}"/>
        <input name="name" placeholder="<fmt:message key="page.admin.label.name" bundle="${rb}"/>" value="<c:out value="${user.name}"/>">
    </label>
    <label>
        <fmt:message key="page.admin.label.surname" bundle="${rb}"/>
        <input name="surname" placeholder="<fmt:message key="page.admin.label.surname" bundle="${rb}"/>" value="<c:out value="${user.surname}"/> ">
    </label>
    <label>
        Email
        <input type="email" name="email" required placeholder="email" value="<c:out value=" ${user.email}"/> ">
    </label>
    <label>
        <fmt:message key="page.user.label.number" bundle="${rb}"/>
        <input name="phone" required placeholder="<fmt:message key="page.user.label.number" bundle="${rb}"/>" value="<c:out value=" ${user.phone}"/> ">
    </label>
    <label>
        <fmt:message key="page.user.label.city" bundle="${rb}"/>
        <input name="city" placeholder="<fmt:message key="page.user.label.city" bundle="${rb}"/>" value="<c:out value=" ${user.city}"/> ">
    </label>
    <input class="button" type="submit" value="<fmt:message key="page.user.label.save" bundle="${rb}"/>">
</form>
<c:import url="components/footer.jsp"/>
</body>
</html>
