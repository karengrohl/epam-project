<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="localizations" var="rb"/>
<html>
<head>
    <title><fmt:message key="page.controller.admin.errorPage" bundle="${rb}"/></title>
</head>
<body>
<c:import url="components/header.jsp"></c:import>
<h1><fmt:message key="page.controller.admin.errorPage" bundle="${rb}"/></h1>
<p>${exceptionMessage}</p>

<c:import url="components/footer.jsp"/>
</body>
</html>
