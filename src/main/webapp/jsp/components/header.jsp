<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="localizations" var="rb"/>
<html>
<head>
    <title>Header</title>
    <link href="<c:url value="/css/style.css"></c:url>" rel="stylesheet" type="text/css">
</head>
<body>

<header class="main_h">
    <div class="row">
        <a class="logo" href="#">B/SH</a>

        <nav>
            <ul>
                <li><a href="/controller/main"><fmt:message key="page.controller.main" bundle="${rb}"/></a></li>
                <c:if test="${user.role == 'USER'}">
                <li><a href="/controller/tariffs"><fmt:message key="page.controller.tariffs" bundle="${rb}"/></a></li>
                <li><a href="/controller/personalinfo"><fmt:message key="path.page.personalinfo.title" bundle="${rb}"/></a></li>
                <li><a href="/controller/user/newpayment"><fmt:message key="path.page.controller.user.newpayment" bundle="${rb}"/></a></li>
                </c:if>
                    <c:if test="${user.role == 'ADMIN'}">
                        <li><a href="/controller/admin/tariffs"><fmt:message key="page.controller.tariffs" bundle="${rb}"/></a></li>
                        <li><a href="/controller/admin/users"><fmt:message key="page.controller.admin.users" bundle="${rb}"/></a></li>
                    </c:if>
                <li>
                    <a href="/controller/logout"><fmt:message key="page.controller.logout" bundle="${rb}"/></a>
                </li>
            </ul>
        </nav>

    </div>

</header>
</body>
</html>
