<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="localizations" var="rb"/>
<html>
<head>
</head>
<body>
<div class="footer">
    <ul class="footer-elements">
        <li><fmt:message key="page.controller.chooseLocal" bundle="${rb}"/></li>
        <li>
            <a href="/controller/changeLocalization?locale=be_BY">BY</a>
            <a href="/controller/changeLocalization?locale=en_US">EN</a>
            <a href="/controller/changeLocalization?locale=ru_RU">РУ</a>
        </li>
    </ul>
</div>
</body>
</html>
