<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="localizations" var="rb"/>
<html>
<head>
    <title>Welcome</title>
</head>
<body>
<c:import url="components/header.jsp"></c:import>
<h1><fmt:message key="page.main.header" bundle="${rb}"/></h1>
<hr/>
<ctg:username/>
<hr/>

<c:if test="${user.role == 'USER'}">
    <c:if test="${tariff == null}">
        <p><fmt:message key="page.main.notFoundTariff" bundle="${rb}"/></p>
    </c:if>
    <c:if test="${tariff != null}">
        <p><fmt:message key="page.main.activeTariff" bundle="${rb}"/> ${tariff.name}</p>
    </c:if>
</c:if>

<c:import url="components/footer.jsp"/>
</body>
</html>