<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setBundle basename="localizations" var="rb"/>
<html>
<head>
    <title>Tariffs</title>
    <link href="<c:url value="/css/style.css"></c:url>" rel="stylesheet" type="text/css">
</head>
<body>
<c:import url="components/header.jsp"></c:import>
<h1>Tariffs</h1>
<c:if test="${subscriptionError}">
    <p>
        Unsufficient funds
    </p>
</c:if>
<table cellspacing="0" cellpadding="0">
    <tr>
        <th><fmt:message key="page.user.label.name" bundle="${rb}"/></th>
        <th><fmt:message key="page.admin.label.speed" bundle="${rb}"/></th>
        <th><fmt:message key="page.admin.label.price" bundle="${rb}"/></th>
    </tr>
    <c:forEach var="tariff" items="${tariffs}">
        <tr>
            <td>${tariff.name}</td>
            <td>${tariff.speed}</td>
            <td>${tariff.price}</td>
            <td><a href="/controller/subscribe/tariff?id=${tariff.id}"><fmt:message key="page.user.connect" bundle="${rb}"/></a></td>
        </tr>
    </c:forEach>
</table>
<c:import url="components/footer.jsp"/>
</body>
</html>
