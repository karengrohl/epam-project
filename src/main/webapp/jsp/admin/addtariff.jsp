<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="localizations" var="rb"/>
<html>
<head>
    <title><fmt:message key="page.admin.tariffInfo" bundle="${rb}"/></title>
    <link href="<c:url value="/css/style.css"></c:url>" rel="stylesheet" type="text/css">
</head>
<body>
<c:import url="../components/header.jsp"></c:import>
<h1>
    <c:if test="${!isEdit}"><fmt:message key="page.admin.newtariff" bundle="${rb}"/></c:if>
    <c:if test="${isEdit}"><fmt:message key="page.admin.edittariff" bundle="${rb}"/></c:if>
</h1>

<form action="/controller/admin/tariffs/save" method="post">
    <input type="hidden" name="id" value="<c:out value="${tariff.id}"/>">
    <input type="hidden" name="isEdit" value="<c:out value="${isEdit}"/>"/>
    <label>
        <fmt:message key="page.admin.label.name" bundle="${rb}"/>
        <input placeholder="Name" required name="name" value="<c:out value="${tariff.name}"/>">
    </label>
    <label>
        <fmt:message key="page.admin.label.speed" bundle="${rb}"/>
        <input type="number" min="0" required placeholder="Speed" name="speed" value="<c:out value="${tariff.speed}"/>">
    </label>
    <label>
        <fmt:message key="page.admin.label.price" bundle="${rb}"/>
        <input type="number" min="0" required placeholder="Price" name="price" value="<c:out value="${tariff.price}"/>">
    </label>
    <input class="button" type="submit" value="<fmt:message key="page.admin.button.save" bundle="${rb}"/>">
</form>
<c:import url="../components/footer.jsp"/>
</body>
</html>
