<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="localizations" var="rb"/>
<html>
<head>
    <title><fmt:message key="page.controller.admin.users" bundle="${rb}"/></title>
    <link href="<c:url value="/css/style.css"></c:url>" rel="stylesheet" type="text/css">
</head>
<body>
<c:import url="../components/header.jsp"></c:import>
<h1><fmt:message key="page.controller.admin.users" bundle="${rb}"/></h1>
<div class="users-tabs">
    <div class="users-tab"><a id="allUsersButton" onclick="showAllUsers()" class="users-tab-active"><fmt:message key="page.controller.admin.activeUsers" bundle="${rb}"/></a></div>
    <div class="users-tab"><a id="blockedUsersButton" onclick="showBlockedUsers()"><fmt:message key="page.controller.admin.blockedUsers" bundle="${rb}"/></a></div>
</div>
<div>
    <div id="allUsers">
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td><fmt:message key="page.controller.admin.id" bundle="${rb}"/></td>
                <td>E-Mail</td>
                <td><fmt:message key="page.admin.label.name" bundle="${rb}"/></td>
                <td><fmt:message key="page.admin.label.surname" bundle="${rb}"/></td>
                <td><fmt:message key="page.admin.label.role" bundle="${rb}"/></td>
                <td><fmt:message key="page.admin.label.balance" bundle="${rb}"/></td>
                <td><fmt:message key="page.admin.label.locking" bundle="${rb}"/></td>
            </tr>
            <c:forEach var="user" items="${users}">
            <tr>
                <td>${user.id}</td>
                <td>${user.email}</td>
                <td>${user.name}</td>
                <td>${user.surname}</td>
                <td>${user.role}</td>
                <td>${user.balance}</td>
                <td>${user.isBlocked}</td>
                <td><a href="/controller/admin/user/block?id=${user.id}"><fmt:message key="page.admin.label.blockUser" bundle="${rb}"/></a></td>
            </tr>
            </c:forEach>
        </table>
    </div>
    <div id="blockedUsers">
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td><fmt:message key="page.controller.admin.id" bundle="${rb}"/></td>
                <td>E-Mail</td>
                <td><fmt:message key="page.admin.label.name" bundle="${rb}"/></td>
                <td><fmt:message key="page.admin.label.surname" bundle="${rb}"/></td>
                <td><fmt:message key="page.admin.label.role" bundle="${rb}"/></td>
                <td><fmt:message key="page.admin.label.balance" bundle="${rb}"/></td>
                <td><fmt:message key="page.admin.label.locking" bundle="${rb}"/></td>
            </tr>
            <c:forEach var="user" items="${blockedUsers}">
                <tr>
                    <td>${user.id}</td>
                    <td>${user.email}</td>
                    <td>${user.name}</td>
                    <td>${user.surname}</td>
                    <td>${user.role}</td>
                    <td>${user.balance}</td>
                    <td>${user.isBlocked}</td>
                    <td><a href="/controller/admin/user/unblock?id=${user.id}"><fmt:message key="page.admin.label.unblock" bundle="${rb}"/></a></td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

<script>

    var allUsers = document.getElementById("allUsers");
    var blockedUsers = document.getElementById("blockedUsers");
    var allUsersButton = document.getElementById("allUsersButton");
    var blockedUsersButton = document.getElementById("blockedUsersButton");

    function showAllUsers() {
        activate(allUsers, allUsersButton);
        deactivate(blockedUsers, blockedUsersButton);
    }

    function showBlockedUsers() {
        activate(blockedUsers, blockedUsersButton);
        deactivate(allUsers, allUsersButton);
    }
    
    function activate(tab, button) {
        tab.style.display = "block";
        button.classList.add("users-tab-active");
    }

    function deactivate(tab, button) {
        tab.style.display = "none";
        button.classList.remove("users-tab-active");
    }

    blockedUsers.style.display = "none";
</script>
<c:import url="../components/footer.jsp"/>
</body>
</html>
