<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="localizations" var="rb"/>
<html>
<head>
    <title><fmt:message key="page.admin.tariffManagement" bundle="${rb}"/></title>
    <link href="<c:url value="/css/style.css"></c:url>" rel="stylesheet" type="text/css">
</head>
<body>
<c:import url="../components/header.jsp"></c:import>
<h1><fmt:message key="page.admin.tariffs" bundle="${rb}"/></h1>
<a href="/controller/admin/tariffs/edit"><fmt:message key="page.admin.newtariff" bundle="${rb}"/></a>
<table cellspacing="0" cellpadding="0">
    <tr>
        <td><fmt:message key="page.admin.label.name" bundle="${rb}"/></td>
        <td><fmt:message key="page.admin.label.speed" bundle="${rb}"/></td>
        <td><fmt:message key="page.admin.label.price" bundle="${rb}"/></td>
        <td><fmt:message key="page.admin.label.change" bundle="${rb}"/></td>
        <td><fmt:message key="page.admin.label.delete" bundle="${rb}"/></td>
    </tr>
    <c:forEach var="tariff" items="${tariffs}">
        <tr>
            <td>${tariff.name}</td>
            <td>${tariff.speed}</td>
            <td>${tariff.price}</td>
            <td><a href="/controller/admin/tariffs/edit?id=${tariff.id}&isEdit=true"><fmt:message key="page.admin.edittariff" bundle="${rb}"/></a></td>
            <td><a href="/controller/admin/tariffs/delete?id=${tariff.id}" onclick="return confirm(<fmt:message key="page.admin.confirm" bundle="${rb}"/>)"><fmt:message key="page.admin.deletetariff" bundle="${rb}"/></a></td>
        </tr>
    </c:forEach>
</table>
<c:import url="../components/footer.jsp"/>
</body>
</html>
