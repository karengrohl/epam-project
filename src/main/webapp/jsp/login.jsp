<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setBundle basename="localizations" var="rb"/>
<html>
<head>
    <title><fmt:message key="page.login.header" bundle="${rb}"/></title>
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/media-queries.css" rel="stylesheet" type="text/css">
</head>
<body id="register-page">
<div class="form">

    <ul class="tab-group">
        <li class="tab" href="#login" onclick="showLogInForm()"><fmt:message key="page.login.header" bundle="${rb}"/></li>
        <li class="tab" href="#signup" onclick="showSignUpForm()"><fmt:message key="page.login.signup" bundle="${rb}"/></li>
    </ul>

    <div class="tab-content" id="login">
    <form name="LoginForm" method="post" action="/controller/login">
        <h1 id="login-text">
            <fmt:message key="page.login.header" bundle="${rb}"/>
        </h1>
        <label>
            EMail </label>
        <input type="email" placeholder="Enter Email" name="email" required>
        <label>
            <fmt:message key="page.login.password" bundle="${rb}"/></label>
        <input type="password" required name="password" placeholder="<fmt:message key="page.login.password" bundle="${rb}"/>" value=""/>
        <br/>
        ${errorLoginPassMessage}
        <br/>
        ${wrongAction}
        <br/>
        ${nullPage}
        <br/>
        <input class="button" id="logInButton" type="submit" value="<fmt:message key="page.login.header" bundle="${rb}"/>"/>
    </form>
        <c:import url="components/footer.jsp"/>
    </div>

    <div class="signup" id="signup">
        <h1 id="login-text"><fmt:message key="page.login.signup" bundle="${rb}"/></h1>
        <form action="/controller/register" method="post">
            <p><fmt:message key="page.login.signupMessage" bundle="${rb}"/></p>
            <div class="container">
                <hr>
                <label><b>Email</b>
                    <input type="email" placeholder="Enter Email" required name="email">
                </label>

                <label><b><fmt:message key="page.login.password" bundle="${rb}"/></b>
                    <input type="password" placeholder="<fmt:message key="page.login.password" bundle="${rb}"/>" required name="password">
                </label>

                <hr>

                <p><fmt:message key="page.login.creatingAccount" bundle="${rb}"/><a href="#"><fmt:message key="page.login.termsPrivacy" bundle="${rb}"/></a>.</p>
                <input class="button" id="signUpButton" type="submit" value="<fmt:message key="page.login.signUpbutton" bundle="${rb}"/>">
            </div>
        </form>
        <c:import url="components/footer.jsp"/>
    </div>


</div>
</body>

<script>

    var logIn = document.getElementById("login");
    var signUp = document.getElementById("signup");
    var signUpButton = document.getElementById("signUpButton");
    var logInButton = document.getElementById("logInButton");

    function showSignUpForm() {
        activate(signUp, signUpButton);
        deactivate(logIn, logInButton);
    }

    function showLogInForm() {
        activate(logIn, logInButton);
        deactivate(signUp, signUpButton);
    }

    function activate(tab, button) {
        tab.style.display = "block";
        button.classList.add("tab-content");
    }

    function deactivate(tab, button) {
        tab.style.display = "none";
        button.classList.remove("tab-content");
    }

    signUp.style.display = "none";

</script>

</html>
