<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="localizations" var="rb"/>
<html>
<head>
    <title><fmt:message key="page.user.replenishment" bundle="${rb}"/></title>
</head>
<body>
<c:import url="../components/header.jsp"></c:import>
<h1><fmt:message key="page.user.replenishment" bundle="${rb}"/></h1>
<form action="/controller/user/newpayment/process" method="post" class="flex-container">
    <label>
        <fmt:message key="page.user.sum" bundle="${rb}"/>
        <input type="number" min="0" required placeholder="Amount" name="amount">
    </label>
    <label>
        <fmt:message key="page.user.promisedPayment" bundle="${rb}"/>
        <input class="checkbox" type="checkbox" name="isPromisedPayment">
    </label>
    <input class="button" type="submit" value="<fmt:message key="page.user.add" bundle="${rb}"/>">
</form>
<hr>
<fmt:message key="page.user.currentBalance" bundle="${rb}"/> ${currentBalance}
<c:import url="../components/footer.jsp"/>
</body>
</html>
