package provider.filters;

import provider.domain.User;
import provider.properties.ConfigurationManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@WebFilter(urlPatterns = "/controller/*", servletNames = {"FrontController"})
public class SecurityFilter implements Filter {
    private static final Set<String> IGNORED_PATHS = new HashSet<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        IGNORED_PATHS.add("/login");
        IGNORED_PATHS.add("/register");
        IGNORED_PATHS.add("/registration");
        IGNORED_PATHS.add("/changeLocalization");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        User user = (User) httpRequest.getSession().getAttribute("user");
        final String pathInfo = httpRequest.getPathInfo();
        if (user == null && !IGNORED_PATHS.contains(pathInfo)) {
            httpResponse.sendRedirect(httpRequest.getContextPath() + ConfigurationManager.getProperty("path.page.index"));
        } else {
            chain.doFilter(request, response);
        }

    }

    @Override
    public void destroy() {

    }
}
