package provider;

import org.apache.log4j.Logger;
import provider.actions.ActionCommand;
import provider.actions.ActionFactory;
import provider.properties.ConfigurationManager;
import provider.properties.MessageManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/controller/*", name = "FrontController")
public class Controller extends HttpServlet {

    private final static Logger LOG = Logger.getLogger(Controller.class);

    private final ActionFactory actionFactory;

    public Controller() {
        actionFactory = new ActionFactory();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String page;
            ActionCommand command = actionFactory.defineCommand(req);
            page = command.execute(req);
            if (page != null) {
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
                dispatcher.forward(req, resp);
            } else {
                page = ConfigurationManager.getProperty("path.page.index");
                resp.sendRedirect(req.getContextPath() + page);
            }
        } catch (Exception exception) {
            LOG.error("Error occurred during processing of request", exception);
            final RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(ConfigurationManager.getProperty("path.page.error"));
            req.setAttribute("exceptionMessage", exception.getMessage());
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }
}
