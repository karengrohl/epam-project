package provider.actions.commands.admin;

import provider.actions.ActionCommand;
import provider.services.SubscriptionService;
import provider.services.SubscriptionServiceImpl;
import provider.services.TariffService;
import provider.services.TariffServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * Action command for tariff deletion
 */
public class DeleteTariffCommand implements ActionCommand{

    private final TariffService tariffService;
    private final SubscriptionService subscriptionService;

    public DeleteTariffCommand() {
        tariffService = new TariffServiceImpl();
        subscriptionService = new SubscriptionServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        final int id = Integer.parseInt(request.getParameter("id"));
        subscriptionService.deleteByTariffId(id);
        tariffService.deleteById(id);
        return "/controller/admin/tariffs";
    }
}
