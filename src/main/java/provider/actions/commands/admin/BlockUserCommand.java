package provider.actions.commands.admin;

import provider.actions.ActionCommand;
import provider.properties.ConfigurationManager;
import provider.services.UserService;
import provider.services.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * Action command for blocking users
 */
public class BlockUserCommand implements ActionCommand {

    private final UserService userService;

    public BlockUserCommand() {
        userService = new UserServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        final int userId = Integer.parseInt(request.getParameter("id"));
        userService.blockUser(userId);
        return "/controller/admin/users";
    }
}
