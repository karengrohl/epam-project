package provider.actions.commands;

import provider.actions.ActionCommand;
import provider.domain.User;
import provider.properties.ConfigurationManager;
import provider.properties.MessageManager;
import provider.services.UserService;
import provider.services.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Action command for login
 */
public class LoginCommand implements ActionCommand {

    private final UserService userService;

    public LoginCommand() {
        userService = new UserServiceImpl();
    }

    public String execute(HttpServletRequest request) throws Exception {
        String page = null;
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        if (userService.checkLogin(email, password)) {
            final Optional<User> optionalUser = userService.findByEmail(email);
            final User user = optionalUser.get();
            user.setPassword("");
            request.getSession().setAttribute("user", user);
            page = "/controller/main";
        } else {
            request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.loginerror"));
            page = ConfigurationManager.getProperty("path.page.login");
        }
        return page;
    }
}
