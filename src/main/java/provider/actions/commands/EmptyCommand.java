package provider.actions.commands;

import provider.actions.ActionCommand;
import provider.properties.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Action command used if specific action command was not found in ActionFactory
 */
public class EmptyCommand implements ActionCommand {
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.login");
        return page;
    }
}
