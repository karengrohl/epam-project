package provider.actions.commands;

import provider.actions.ActionCommand;
import provider.domain.Role;
import provider.domain.User;
import provider.mappers.request.RequestMapper;
import provider.mappers.request.UserMapper;
import provider.properties.ConfigurationManager;
import provider.services.UserService;
import provider.services.UserServiceImpl;
import provider.validation.ValidationException;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

/**
 * Action command for registration of new user
 */
public class RegisterCommand implements ActionCommand {

    private final RequestMapper<User> mapper;
    private final UserService userService;

    public RegisterCommand() {
        mapper = new UserMapper();
        userService = new UserServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws ValidationException, SQLException {
        final User user = mapper.map(request);
        user.setRole(Role.USER);
        userService.saveToDatabase(user);
        return ConfigurationManager.getProperty("path.page.index");
    }
}
