package provider.actions.commands;

import provider.actions.ActionCommand;
import provider.domain.User;
import provider.properties.ConfigurationManager;
import provider.services.SessionService;
import provider.services.SessionServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * Action command for localization changing
 */
public class ChangeLocalizationCommand implements ActionCommand {

    private final SessionService sessionService;

    public ChangeLocalizationCommand() {
        sessionService = new SessionServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        final String locale = request.getParameter("locale");
        sessionService.setLocalization(request, locale);
        final User currentUser = sessionService.getCurrentUser(request);
        if (currentUser != null) {
            return "/controller/main";
        } else {
            return ConfigurationManager.getProperty("path.page.index");
        }
    }
}
