package provider.actions.commands;

import provider.domain.User;
import provider.mappers.request.RequestMapper;
import provider.mappers.request.UserMapper;
import provider.properties.ConfigurationManager;
import provider.services.UserService;
import provider.services.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Action command for saving of personal info
 */
public class PersonalInfoSaveCommand implements provider.actions.ActionCommand {

    private final UserService userService;
    private final RequestMapper<User> userMapper;

    public PersonalInfoSaveCommand() {
        userService = new UserServiceImpl();
        userMapper = new UserMapper();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        final User userInfo = userMapper.map(request);
        final HttpSession session = request.getSession();
        final User currentUser = (User) session.getAttribute("user");
        userService.updatePersonalInformation(currentUser.getId(), userInfo);
        final User updatedUser = userService.findByEmail(userInfo.getEmail()).get();
        session.setAttribute("user", updatedUser);
        return "/controller/main";
    }
}
