package provider.actions.commands;

import provider.actions.ActionCommand;
import provider.domain.Transaction;
import provider.domain.User;
import provider.mappers.request.RequestMapper;
import provider.mappers.request.TransactionMapper;
import provider.properties.ConfigurationManager;
import provider.services.TransactionService;
import provider.services.TransactionServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * Action command for processing of user payment
 */
public class ProcessNewPaymentCommand implements ActionCommand {

    private final RequestMapper<Transaction> transactionMapper;
    private final TransactionService transactionService;

    public ProcessNewPaymentCommand() {
        transactionMapper = new TransactionMapper();
        transactionService = new TransactionServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        final Transaction transaction = transactionMapper.map(request);
        boolean isPromisedPayment = "on".equals(request.getParameter("isPromisedPayment"));
        final User user = (User) request.getSession().getAttribute("user");
        transactionService.processNewPayment(user, transaction, isPromisedPayment);
        return "/controller/main";
    }
}
