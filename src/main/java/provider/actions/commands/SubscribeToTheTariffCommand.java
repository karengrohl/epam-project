package provider.actions.commands;

import provider.domain.Tariff;
import provider.domain.User;
import provider.properties.ConfigurationManager;
import provider.services.*;
import provider.services.exceptions.SubscriptionException;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

/**
 * Action command for user subscription to tariff
 */
public class SubscribeToTheTariffCommand implements provider.actions.ActionCommand {

    private final SubscriptionService subscriptionService;
    private final TariffService tariffService;
    private final UserService userService;

    public SubscribeToTheTariffCommand() {
        subscriptionService = new SubscriptionServiceImpl();
        tariffService = new TariffServiceImpl();
        userService = new UserServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {

        final int tariffId = Integer.parseInt(request.getParameter("id"));
        User user = (User) request.getSession().getAttribute("user");
        user = userService.findById(user.getId());

        final Tariff tariff = tariffService.findTariffById(tariffId).get();

        try {
            subscriptionService.subscribeUserToTariff(user, tariff);
        } catch (SubscriptionException e) {
            request.setAttribute("subscriptionError", true);
            return "/controller/tariffs";
        }

        return "/controller/main";
    }
}
