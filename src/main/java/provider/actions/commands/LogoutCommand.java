package provider.actions.commands;

import provider.actions.ActionCommand;
import provider.properties.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Action command for logout
 */
public class LogoutCommand implements ActionCommand {
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.index");
        request.getSession().invalidate();
        return page;
    }
}
