package provider.actions.commands;

import com.sun.org.apache.xpath.internal.operations.Bool;
import provider.actions.ActionCommand;
import provider.domain.Tariff;
import provider.mappers.request.RequestMapper;
import provider.mappers.request.TariffMapper;
import provider.services.TariffService;
import provider.services.TariffServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * Action command for saving or updating of tariff
 */
public class SaveOrUpdateNewTariffCommand implements ActionCommand {

    private final TariffService tariffService;
    private final RequestMapper<Tariff> mapper;

    public SaveOrUpdateNewTariffCommand() {
        mapper = new TariffMapper();
        tariffService = new TariffServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        final Tariff tariff = mapper.map(request);
        boolean isEdit = Boolean.parseBoolean(request.getParameter("isEdit"));
        if (isEdit) {
            int id = Integer.parseInt(request.getParameter("id"));
            tariffService.update(id, tariff);
        } else {
            tariffService.saveToDatabase(tariff);
        }
        return "/controller/admin/tariffs";
    }
}
