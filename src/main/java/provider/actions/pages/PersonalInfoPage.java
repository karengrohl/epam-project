package provider.actions.pages;

import provider.actions.ActionCommand;
import provider.properties.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Action command for personal information modification page
 */
public class PersonalInfoPage implements ActionCommand{

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        return ConfigurationManager.getProperty("path.page.personalinfo");
    }
}
