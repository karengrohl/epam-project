package provider.actions.pages;

import provider.actions.ActionCommand;
import provider.domain.User;
import provider.properties.ConfigurationManager;
import provider.services.UserService;
import provider.services.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Action command for user administration page
 */
public class UsersPage implements ActionCommand {

    private final UserService userService;

    public UsersPage() {
        userService = new UserServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        final List<User> users = userService.findAll();
        List<User> blockedUsers = filterBlockedUsers(users);
        final List<User> activeUsers = filterActiveUsers(users);
        request.setAttribute("users", activeUsers);
        request.setAttribute("blockedUsers", blockedUsers);
        return ConfigurationManager.getProperty("path.page.admin.users");
    }

    private List<User> filterActiveUsers(List<User> users) {
        return users
                .stream()
                .filter(user -> !user.getIsBlocked())
                .collect(Collectors.toList());
    }

    private List<User> filterBlockedUsers(List<User> users) {
        return users
                .stream()
                .filter(user -> user.getIsBlocked())
                .collect(Collectors.toList());
    }
}
