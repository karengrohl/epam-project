package provider.actions.pages;

import provider.actions.ActionCommand;
import provider.domain.Tariff;
import provider.properties.ConfigurationManager;
import provider.services.TariffService;
import provider.services.TariffServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Action command for tariff modification page
 */
public class AddOrUpdateTariffPage implements ActionCommand {

    private final TariffService tariffService;

    public AddOrUpdateTariffPage() {
        tariffService = new TariffServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        boolean isEdit = Boolean.parseBoolean(request.getParameter("isEdit"));
        if (isEdit) {
            request.setAttribute("isEdit", true);
            final int id = Integer.parseInt(request.getParameter("id"));
            final Optional<Tariff> tariff = tariffService.findTariffById(id);
            if (tariff.isPresent()) {
                request.setAttribute("tariff", tariff.get());
            } else {
                throw new IllegalArgumentException("Tariff with id " + id + " is not found");
            }
        }
        return ConfigurationManager.getProperty("path.page.admin.tariffs.new");
    }
}
