package provider.actions.pages;

import provider.actions.ActionCommand;
import provider.domain.User;
import provider.properties.ConfigurationManager;
import provider.services.UserService;
import provider.services.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * Action command for payment page
 */
public class NewPaymentPage implements ActionCommand {

    private final UserService userService;

    public NewPaymentPage() {
        userService = new UserServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        final User user = (User) request.getSession().getAttribute("user");
        final User actualUser = userService.findById(user.getId());
        request.setAttribute("currentBalance", actualUser.getBalance());
        return ConfigurationManager.getProperty("path.page.user.newpayment");
    }
}
