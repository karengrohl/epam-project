package provider.actions.pages;

import provider.actions.ActionCommand;
import provider.domain.Subscription;
import provider.domain.Tariff;
import provider.domain.User;
import provider.properties.ConfigurationManager;
import provider.services.SubscriptionService;
import provider.services.SubscriptionServiceImpl;
import provider.services.TariffService;
import provider.services.TariffServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Action command for main page of the application
 */
public class MainPage implements ActionCommand {
    private final SubscriptionService subscriptionService;
    private final TariffService tariffService;

    public MainPage(){
        subscriptionService = new SubscriptionServiceImpl();
        tariffService = new TariffServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        final User user = (User) request.getSession().getAttribute("user");
        Optional<Subscription> activeSubscription = subscriptionService.findActiveSubscription(user);
        if (activeSubscription.isPresent()) {
            int tariffId = activeSubscription.get().getTariffId();

            final Optional<Tariff> optionalTariff = tariffService.findTariffById(tariffId);
            if (optionalTariff.isPresent()) {
                Tariff tariff = optionalTariff.get();
                request.setAttribute("tariff", tariff);
            }
        }
        return ConfigurationManager.getProperty("path.page.main");
    }
}
