package provider.actions.pages;

import provider.actions.ActionCommand;
import provider.dao.TariffDAO;
import provider.dao.TariffDAOImpl;
import provider.domain.Tariff;
import provider.properties.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Action command for tariffs page
 */
public class TariffsPage implements ActionCommand {

    private final TariffDAO tariffDAO;

    public TariffsPage() {
        tariffDAO = new TariffDAOImpl();
    }

    @Override
    public String execute(HttpServletRequest request) throws Exception {
        final List<Tariff> tariffs = tariffDAO.findAll();
        request.setAttribute("tariffs", tariffs);
        return ConfigurationManager.getProperty("path.page.tariffs");
    }
}
