package provider.actions;

import provider.actions.commands.*;
import provider.actions.commands.admin.BlockUserCommand;
import provider.actions.commands.admin.DeleteTariffCommand;
import provider.actions.commands.admin.UnblockUserCommand;
import provider.actions.pages.*;
import provider.properties.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for defining of action command based on http request information
 */
public class ActionFactory {
    private static Map<String, ActionCommand> MAPPINGS;

    public ActionFactory() {
        createMappings();
    }

    private void createMappings() {
        MAPPINGS = new HashMap<>();
        MAPPINGS.put("/login", new LoginCommand());
        MAPPINGS.put("/logout", new LogoutCommand());
        MAPPINGS.put("/tariffs", new TariffsPage());
        MAPPINGS.put("/main", new MainPage());
        MAPPINGS.put("/admin/tariffs", new AdminTariffsPage());
        MAPPINGS.put("/admin/tariffs/edit", new AddOrUpdateTariffPage());
        MAPPINGS.put("/admin/tariffs/save", new SaveOrUpdateNewTariffCommand());
        MAPPINGS.put("/admin/tariffs/delete", new DeleteTariffCommand());
        MAPPINGS.put("/registration", new RegistrationPage());
        MAPPINGS.put("/register", new RegisterCommand());
        MAPPINGS.put("/subscribe/tariff", new SubscribeToTheTariffCommand());
        MAPPINGS.put("/personalinfo", new PersonalInfoPage());
        MAPPINGS.put("/personalinfo/save", new PersonalInfoSaveCommand());
        MAPPINGS.put("/user/newpayment", new NewPaymentPage());
        MAPPINGS.put("/user/newpayment/process", new ProcessNewPaymentCommand());
        MAPPINGS.put("/admin/users", new UsersPage());
        MAPPINGS.put("/admin/user/block", new BlockUserCommand());
        MAPPINGS.put("/admin/user/unblock", new UnblockUserCommand());
        MAPPINGS.put("/changeLocalization", new ChangeLocalizationCommand());
    }

    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = null;
        String action = request.getPathInfo();
        try {
            current = MAPPINGS.get(action);
            if (current == null) {
                return new EmptyCommand();
            }
        } catch (IllegalArgumentException e) {
            request.setAttribute("wrongAction", action + MessageManager.getProperty("message.wrongaction"));
        }
        return current;
    }
}
