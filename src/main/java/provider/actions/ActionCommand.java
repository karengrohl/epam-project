package provider.actions;

import javax.servlet.http.HttpServletRequest;

/**
 * Interface for execution of specific behavior based on http request information
 */
public interface ActionCommand {
    /**
     * Execution of specific behavior and return path for page to redirect
     * @param request
     * @return path for page to redirect
     * @throws Exception
     */
    String execute(HttpServletRequest request) throws Exception;
}
