package provider.validation;

public abstract class DefaultValidator<T> implements Validator<T> {

    public void validateMaxLength(String string, int length) throws ValidationException {
        if (string.length() > length) {
            throw new ValidationException("Length exceeds allowed value: " + length);
        }
    }

    public void validateMinLength(String string, int length) throws ValidationException {
        if (string.length() < length) {
            throw new ValidationException("Length is less than the allowable value: " + length);
        }
    }

    public void validateLength(String string, int minLength, int maxLength) throws ValidationException {
        validateMinLength(string, minLength);
        validateMaxLength(string, maxLength);
    }

}
