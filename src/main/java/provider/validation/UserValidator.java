package provider.validation;

import provider.domain.User;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class UserValidator extends DefaultValidator<User> {

    @Override
    public void validate(User user) throws ValidationException {
        validateEmail(user);
        if (user.getName() != null) {
            validateMaxLength(user.getName(), 50);
            validateMaxLength(user.getSurname(), 50);
        }
    }

    private void validateEmail(User user) throws ValidationException {
        try {
            final InternetAddress emailAddress = new InternetAddress(user.getEmail());
            emailAddress.validate();
        } catch (AddressException e) {
            throw new ValidationException("Email is not valid");
        }
    }
}
