package provider.validation;

import provider.domain.Tariff;

public class TariffValidator extends DefaultValidator<Tariff> {
    @Override
    public void validate(Tariff tariff) throws ValidationException {
        validateLength(tariff.getName(), 2, 20);
    }
}
