package provider.dao;

import provider.domain.Tariff;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Data access object for tariff manipulations
 */
public interface TariffDAO {
    /**
     * Retrieve all tariffs from database
     * @return List of all tariffs
     * @throws SQLException
     */
    List<Tariff> findAll() throws SQLException;

    /**
     * Add tariff to database
     * @param tariff
     * @throws SQLException
     */
    void save(Tariff tariff) throws SQLException;

    /**
     * Find tariff by id
     * @param id
     * @return Empty Optional or Optional with found tariff
     * @throws SQLException
     */
    Optional<Tariff> findById(int id) throws SQLException;

    /**
     * Update tariff record by tariff id
     * @param id
     * @param tariff
     * @throws SQLException
     */
    void update(int id, Tariff tariff) throws SQLException;

    /**
     * Delete tariff by ID
     * @param id
     * @throws SQLException
     */
    void deleteById(int id) throws SQLException;
}
