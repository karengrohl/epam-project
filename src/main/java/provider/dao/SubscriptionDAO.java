package provider.dao;

import provider.domain.Subscription;
import provider.domain.User;

import java.util.Date;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Data access object for subscription manipulations
 */
public interface SubscriptionDAO {
    /**
     * Save subscription to database
     * @param subscription
     * @throws SQLException
     */
    void save(Subscription subscription) throws SQLException;

    /**
     * Set new end date for subscriptions with specified userId
     * @param userId
     * @param endDate
     * @throws SQLException
     */
    void updateEndDateByUserId(int userId, Date endDate) throws SQLException;

    /**
     * Returns current subscription for userId
     * @param userId
     * @return Current user's subscription
     * @throws SQLException
     */
    Optional<Subscription> getCurrentSubscription(int userId) throws SQLException;

    /**
     * Delete subscriptions by tariffId
     * @param id
     * @throws SQLException
     */
    void deleteByTariffId(int id) throws SQLException;
}
