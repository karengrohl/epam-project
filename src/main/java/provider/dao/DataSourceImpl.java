package provider.dao;

import provider.properties.ConfigurationManager;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Data source for database
 */
public class DataSourceImpl implements DataSource {

    private final ConnectionPool connectionPool;
    private static DataSourceImpl instance;

    private DataSourceImpl() {
        connectionPool = new ConnectionPoolImpl(
                ConfigurationManager.getProperty("datasource.username"),
                ConfigurationManager.getProperty("datasource.password"),
                ConfigurationManager.getProperty("datasource.url"),
                Integer.parseInt(ConfigurationManager.getProperty("datasource.poolSize"))
        );
    }

    public static DataSourceImpl getInstance() {
        if (instance == null) {
            instance = new DataSourceImpl();
            return instance;
        } else {
            return instance;
        }
    }

    @Override
    public void returnConnection(Connection connection) throws SQLException {
        this.connectionPool.returnConnection(connection);
    }

    @Override
    public Connection getConnection() throws SQLException {
        return this.connectionPool.getConnection();
    }
}
