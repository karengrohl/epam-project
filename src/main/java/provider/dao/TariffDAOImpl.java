package provider.dao;

import provider.domain.Tariff;
import provider.mappers.database.ResultSetMapper;
import provider.mappers.database.TariffMapper;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Data access object for tariff manipulations
 */
public class TariffDAOImpl implements TariffDAO {

    private final ResultSetMapper<Tariff> mapper;
    private final DataSourceImpl dataSource;

    public TariffDAOImpl() {
        mapper = new TariffMapper();
        dataSource = DataSourceImpl.getInstance();
    }

    @Override
    public List<Tariff> findAll() throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery("SELECT * FROM tariff");
            List<Tariff> tariffs = new LinkedList<>();
            while (resultSet.next()) {
                final Tariff tariff = mapper.map(resultSet);
                tariffs.add(tariff);
            }
            return tariffs;
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public void save(Tariff tariff) throws SQLException {
        Connection connection = null;
        try  {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("INSERT INTO tariff (name, price, speed) VALUES (?, ?, ?);");
            statement.setString(1, tariff.getName());
            statement.setDouble(2, tariff.getPrice());
            statement.setDouble(3, tariff.getSpeed());
            statement.executeUpdate();
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public Optional<Tariff> findById(int id) throws SQLException {
        Connection connection = null;
        try  {
            connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM tariff WHERE tariff.id = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(mapper.map(resultSet));
            }
            return Optional.empty();
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public void update(int id, Tariff tariff) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("UPDATE tariff SET name = ?, price = ?, speed = ? WHERE id = ?");
            statement.setString(1, tariff.getName());
            statement.setDouble(2, tariff.getPrice());
            statement.setDouble(3, tariff.getSpeed());
            statement.setInt(4, id);
            statement.executeUpdate();
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public void deleteById(int id) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("DELETE FROM tariff WHERE id = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } finally {
            dataSource.returnConnection(connection);
        }
    }
}
