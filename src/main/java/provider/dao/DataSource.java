package provider.dao;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Data source for database
 */
public interface DataSource {
    /**
     * Acquire connection
     * @return Connection to database
     * @throws SQLException
     */
    Connection getConnection() throws SQLException;

    /**
     * Return connection
     * @param connection
     * @throws SQLException
     */
    void returnConnection(Connection connection) throws SQLException;
}
