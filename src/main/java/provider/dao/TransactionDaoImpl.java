package provider.dao;

import provider.domain.Transaction;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Data access object for finance transactions
 */
public class TransactionDaoImpl implements TransactionDao {

    private final DataSourceImpl dataSource;

    public TransactionDaoImpl() {
        dataSource = DataSourceImpl.getInstance();
    }

    @Override
    public void save(Transaction transaction) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("INSERT INTO transaction (amount, date, type, user_id) VALUES (?, ?, ?, ?)");
            statement.setDouble(1, transaction.getAmount());
            statement.setDate(2, convertDate(transaction.getDate()));
            statement.setString(3, transaction.getType().toString());
            statement.setInt(4, transaction.getUserId());
            statement.executeUpdate();
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    private Date convertDate(LocalDateTime date) {
        return new Date(date.atZone(ZoneOffset.UTC).toInstant().toEpochMilli());
    }
}
