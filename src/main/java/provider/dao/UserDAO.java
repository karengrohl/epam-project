package provider.dao;

import provider.domain.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Data access object for user manipulations
 */
public interface UserDAO {
    /**
     * Find user by email
     * @param email
     * @return Empty Optional or Optional with found user
     * @throws SQLException
     */
    Optional<User> findByEmail(String email) throws SQLException;

    /**
     * Save user to database
     * @param user
     * @throws SQLException
     */
    void save(User user) throws SQLException;

    /**
     * Update personal information of user by userId
     * @param id
     * @param user
     * @throws SQLException
     */
    void updatePersonalInformation(int id, User user) throws SQLException;

    /**
     * Find user by id
     * @param userId
     * @return Empty Optional or Optional with found user
     * @throws SQLException
     */
    Optional<User> findById(int userId) throws SQLException;

    /**
     * Update user's balance by user id
     * @param userId
     * @param newBalance
     * @throws SQLException
     */
    void updateBalance(int userId, double newBalance) throws SQLException;

    /**
     * Find all users
     * @return List of all users
     * @throws SQLException
     */
    List<User> findAll() throws SQLException;

    /**
     * Change isBlocked status of user with specified userId
     * @param userId
     * @param isBlocked
     * @throws SQLException
     */
    void changeIsBlocked(int userId, boolean isBlocked) throws SQLException;
}
