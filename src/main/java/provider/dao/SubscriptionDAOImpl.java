package provider.dao;

import provider.domain.Subscription;
import provider.mappers.database.ResultSetMapper;
import provider.mappers.database.SubscriptionMapper;


import java.sql.*;
import java.util.Optional;

/**
 * Data access object for subscription manipulations
 */
public class SubscriptionDAOImpl implements SubscriptionDAO {

    private final DataSourceImpl dataSource;
    private final ResultSetMapper<Subscription> mapper;

    public SubscriptionDAOImpl() {
        dataSource = DataSourceImpl.getInstance();
        mapper = new SubscriptionMapper();
    }

    @Override
    public void save(Subscription subscription) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("INSERT INTO subscription (user_id, tariff_id, startDate) VALUES (?, ?, ?)");
            statement.setInt(1, subscription.getUserId());
            statement.setInt(2, subscription.getTariffId());
            statement.setDate(3, new Date(subscription.getStartDate().getTime()));
            statement.executeUpdate();
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public void updateEndDateByUserId(int userId, java.util.Date endDate) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("UPDATE subscription SET endDate = ? WHERE user_id = ? AND endDate IS NULL");
            statement.setDate(1, new Date(endDate.getTime()));
            statement.setInt(2, userId);
            statement.executeUpdate();
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public Optional<Subscription> getCurrentSubscription(int userId) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("SELECT * FROM subscription WHERE user_id = ? AND endDate IS NULL ");
            statement.setInt(1, userId);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(mapper.map(resultSet));
            }
        } finally {
            dataSource.returnConnection(connection);
        }
        return Optional.empty();
    }

    @Override
    public void deleteByTariffId(int id) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("DELETE FROM subscription WHERE tariff_id = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } finally {
            dataSource.returnConnection(connection);
        }
    }
}
