package provider.dao;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Connection pool for managing of connections
 */
public interface ConnectionPool {
    /**
     * Method for connection acquiring
     * @return Connection from pool
     * @throws SQLException
     */
    Connection getConnection() throws SQLException;

    /**
     * Method for returning of the connection to connection pool
     * @param connection
     * @throws SQLException
     */
    void returnConnection(Connection connection) throws SQLException;
}
