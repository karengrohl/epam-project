package provider.dao;

import provider.domain.Transaction;

import java.sql.SQLException;

/**
 * Data access object for finance transactions
 */
public interface TransactionDao {

    /**
     * Save transaction to database
     * @param transaction
     * @throws SQLException
     */
    void save(Transaction transaction) throws SQLException;
}
