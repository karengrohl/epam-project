package provider.dao;

import com.mysql.jdbc.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Connection pool for managing of connections
 */
public class ConnectionPoolImpl implements ConnectionPool {

    private final Deque<Connection> freeConnections = new LinkedList<>();
    private final Deque<Connection> usedConnections = new LinkedList<>();
    private final String username;
    private final String password;
    private final String url;
    private final int poolSize;
    private final Lock getConnectionLock = new ReentrantLock();
    private final Lock returnConnectionLock = new ReentrantLock();
    private final AtomicInteger numberOfFreeConnections ;


    public ConnectionPoolImpl(String username, String password, String url, int poolSize) {
        this.username = username;
        this.password = password;
        this.url = url;
        this.poolSize = poolSize;
        initConnectionPool();
        this.numberOfFreeConnections = new AtomicInteger(poolSize);
    }

    private void initConnectionPool() {
        try {
            DriverManager.registerDriver(new Driver());
            for (int i = 0; i < poolSize; i++) {
                freeConnections.addFirst(createConnection());
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    @Override
    public Connection getConnection() throws SQLException {
        getConnectionLock.lock();
        try {
            Connection connection = null;
            if (numberOfFreeConnections.get() == 0) {
                connection = createConnection();
            } else {
                connection = freeConnections.removeFirst();
                usedConnections.addFirst(connection);
                numberOfFreeConnections.decrementAndGet();
            }
            return connection;
        } finally {
            getConnectionLock.unlock();
        }
    }

    @Override
    public void returnConnection(Connection connection) throws SQLException {
        returnConnectionLock.lock();
        try {
            if (connection != null) {
                if (usedConnections.remove(connection)) {
                    freeConnections.addFirst(connection);
                    numberOfFreeConnections.incrementAndGet();
                } else {
                    connection.close();
                }
            }
        } finally {
            returnConnectionLock.unlock();
        }

    }
}
