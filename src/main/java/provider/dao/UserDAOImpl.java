package provider.dao;

import provider.domain.User;
import provider.mappers.database.ResultSetMapper;
import provider.mappers.database.UserMapper;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Data access object for user manipulations
 */
public class UserDAOImpl implements UserDAO {

    private final ResultSetMapper<User> mapper;
    private final DataSource dataSource;

    public UserDAOImpl() {
        mapper = new UserMapper();
        dataSource = DataSourceImpl.getInstance();
    }

    @Override
    public Optional<User> findByEmail(String email) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("SELECT * FROM user WHERE user.email = ?");
            statement.setString(1, email);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(mapper.map(resultSet));
            }
            return Optional.empty();
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public void save(User user) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("INSERT INTO user (email, password, role) VALUES (?, ?, ?)");
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getRole().toString());
            statement.executeUpdate();
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public void updatePersonalInformation(int id, User userInfo) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("UPDATE user SET name = ?, surname = ?, email = ?, phone = ?, city = ? WHERE id = ?");
            statement.setString(1, userInfo.getName());
            statement.setString(2, userInfo.getSurname());
            statement.setString(3, userInfo.getEmail());
            statement.setString(4, userInfo.getPhone());
            statement.setString(5, userInfo.getCity());
            statement.setInt(6, id);
            statement.executeUpdate();
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public Optional<User> findById(int userId) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("SELECT * FROM user WHERE user.id = ?");
            statement.setInt(1, userId);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(mapper.map(resultSet));
            }
            return Optional.empty();
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public void updateBalance(int userId, double newBalance) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("UPDATE user SET balance = ? WHERE id = ?");
            statement.setDouble(1, newBalance);
            statement.setInt(2, userId);
            statement.executeUpdate();
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public List<User> findAll() throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery("SELECT * FROM user");
            List<User> users = new LinkedList<>();
            while (resultSet.next()) {
                final User user = mapper.map(resultSet);
                users.add(user);
            }
            return users;
        } finally {
            dataSource.returnConnection(connection);
        }
    }

    @Override
    public void changeIsBlocked(int userId, boolean isBlocked) throws SQLException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement statement = connection.prepareStatement("UPDATE user SET isBlocked = ? WHERE id = ?");
            statement.setBoolean(1, isBlocked);
            statement.setInt(2, userId);
            statement.executeUpdate();
        } finally {
            dataSource.returnConnection(connection);
        }
    }
}
