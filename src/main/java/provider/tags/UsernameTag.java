package provider.tags;

import provider.domain.User;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class UsernameTag extends TagSupport {
    @Override
    public int doStartTag() throws JspException {

        final User user = (User) pageContext.getSession().getAttribute("user");
        try {
            JspWriter out = pageContext.getOut();
            out.write(user.getEmail());
        } catch (IOException e) {
            throw new JspException(e.getMessage(), e);
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
