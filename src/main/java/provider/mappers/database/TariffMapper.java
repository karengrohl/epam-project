package provider.mappers.database;

import provider.domain.Tariff;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TariffMapper implements ResultSetMapper<Tariff> {
    @Override
    public Tariff map(ResultSet resultSet) throws SQLException {

        final int id = resultSet.getInt("id");
        final String name = resultSet.getString("name");
        final double price = resultSet.getDouble("price");
        final double speed = resultSet.getDouble("speed");

        return new Tariff(id, name, price, speed);
    }
}
