package provider.mappers.database;

import provider.domain.Subscription;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class SubscriptionMapper implements ResultSetMapper<Subscription> {
    @Override
    public Subscription map(ResultSet resultSet) throws SQLException {

        final int id = resultSet.getInt("id");
        final int userId = resultSet.getInt("user_id");
        final int tariffId = resultSet.getInt("tariff_id");
        final Date startDate = resultSet.getDate("startDate");
        final Date endDate = resultSet.getDate("endDate");
        return new Subscription(id, tariffId, userId, startDate, endDate);
    }
}
