package provider.mappers.database;

import provider.domain.Role;
import provider.domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements ResultSetMapper<User> {

    @Override
    public User map(ResultSet resultSet) throws SQLException {
        final int id = resultSet.getInt("id");
        final String email = resultSet.getString("email");
        final String password = resultSet.getString("password");
        final String name = resultSet.getString("name");
        final String surname = resultSet.getString("surname");
        final Role role = Role.valueOf(resultSet.getString("role").toUpperCase());
        final double balance = resultSet.getDouble("balance");
        final boolean isBlocked = resultSet.getBoolean("isBlocked");
        final String phone = resultSet.getString("phone");
        final String city = resultSet.getString("city");
        final User user = new User(id, email, name, surname, password, role, phone, city);
        user.setBalance(balance);
        user.setBlocked(isBlocked);
        return user;
    }
}
