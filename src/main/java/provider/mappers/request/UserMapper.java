package provider.mappers.request;

import provider.domain.User;
import provider.validation.UserValidator;
import provider.validation.ValidationException;
import provider.validation.Validator;

import javax.servlet.http.HttpServletRequest;

public class UserMapper implements RequestMapper<User>{

    private final Validator<User> validator;

    public UserMapper() {
        validator = new UserValidator();
    }

    @Override
    public User map(HttpServletRequest request) throws ValidationException {

        final String name = request.getParameter("name");
        final String surname = request.getParameter("surname");
        final String email = request.getParameter("email");
        final String password = request.getParameter("password");
        final String phone = request.getParameter("phone");
        final String city = request.getParameter("city");
        final User user = new User(email, name, surname, password, phone, city);
        validator.validate(user);
        return user;
    }
}
