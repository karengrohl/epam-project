package provider.mappers.request;

import provider.validation.ValidationException;

import javax.servlet.http.HttpServletRequest;

public interface RequestMapper<T> {

    T map(HttpServletRequest request) throws ValidationException;

}
