package provider.mappers.request;

import provider.domain.Tariff;
import provider.validation.TariffValidator;
import provider.validation.ValidationException;
import provider.validation.Validator;

import javax.servlet.http.HttpServletRequest;

public class TariffMapper implements RequestMapper<Tariff> {

    private final Validator<Tariff> validator;

    public TariffMapper() {
        validator = new TariffValidator();
    }

    @Override
    public Tariff map(HttpServletRequest request) throws ValidationException {
        final String name = request.getParameter("name");
        final double price = Double.parseDouble(request.getParameter("price"));
        final double speed = Double.parseDouble(request.getParameter("speed"));
        final Tariff tariff = new Tariff(name, price, speed);
        validator.validate(tariff);
        return tariff;
    }
}
