package provider.mappers.request;

import provider.domain.Transaction;

import javax.servlet.http.HttpServletRequest;

public class TransactionMapper implements RequestMapper<Transaction> {
    @Override
    public Transaction map(HttpServletRequest request) {
        final double amount = Double.parseDouble(request.getParameter("amount"));
        final Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        return transaction;
    }
}
