package provider.domain;

import java.util.Date;

/**
 * Subscription entity which represents relations between user and subscribed tariff
 */
public class Subscription {
    private int id;
    private int tariffId;
    private int userId;
    private Date startDate;
    private Date endDate;

    public Subscription() {
    }

    public Subscription(int tariffId, int userId, Date startDate) {
        this.tariffId = tariffId;
        this.userId = userId;
        this.startDate = startDate;
    }

    public Subscription(int id, int tariffId, int userId, Date startDate, Date endDate) {
        this.id = id;
        this.tariffId = tariffId;
        this.userId = userId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Subscription)) return false;

        Subscription that = (Subscription) o;

        if (id != that.id) return false;
        if (tariffId != that.tariffId) return false;
        if (userId != that.userId) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + tariffId;
        result = 31 * result + userId;
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        return result;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTariffId() {
        return tariffId;
    }

    public void setTariffId(int tariffId) {
        this.tariffId = tariffId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
