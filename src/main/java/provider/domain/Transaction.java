package provider.domain;

import java.time.LocalDateTime;

/**
 * Transaction entity for storing information about user's balance changes
 */
public class Transaction {

    private int id;
    private double amount;
    private LocalDateTime date;
    private Type type;
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public static enum Type {
        PAYMENT, WITHDRAWAL, PROMISED_PAYMENT
    }

    public Transaction(int id, double amount, LocalDateTime date, Type type, int userId) {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.type = type;
        this.userId = userId;
    }

    public Transaction() {
    }

    public Transaction(double amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;

        Transaction that = (Transaction) o;

        if (getId() != that.getId()) return false;
        if (Double.compare(that.getAmount(), getAmount()) != 0) return false;
        if (getUserId() != that.getUserId()) return false;
        if (getDate() != null ? !getDate().equals(that.getDate()) : that.getDate() != null) return false;
        if (getType() != that.getType()) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getId();
        temp = Double.doubleToLongBits(getAmount());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getDate() != null ? getDate().hashCode() : 0);
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + getUserId();
        return result;
    }
}
