package provider.domain;

/**
 * Providers tariff entity
 */
public class Tariff {
    private int id;
    private String name;
    private double price;
    private double speed;

    public Tariff() {
    }

    public Tariff(String name, double price, double speed) {
        this.name = name;
        this.price = price;
        this.speed = speed;
    }

    public Tariff(int id, String name, double price, double speed) {

        this.id = id;
        this.name = name;
        this.price = price;
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", speed='" + speed + '\'' +
                '}';
    }


    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tariff)) return false;

        Tariff tariff = (Tariff) o;

        if (getId() != tariff.getId()) return false;
        if (Double.compare(tariff.getPrice(), getPrice()) != 0) return false;
        if (Double.compare(tariff.getSpeed(), getSpeed()) != 0) return false;
        if (getName() != null ? !getName().equals(tariff.getName()) : tariff.getName() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getId();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        temp = Double.doubleToLongBits(getPrice());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getSpeed());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
