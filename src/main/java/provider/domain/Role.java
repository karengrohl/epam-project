package provider.domain;

/**
 * User roles
 */
public enum Role {
    ADMIN, USER
}
