package provider.services;

import provider.dao.SubscriptionDAO;
import provider.dao.SubscriptionDAOImpl;
import provider.domain.Subscription;
import provider.domain.Tariff;
import provider.domain.Transaction;
import provider.domain.User;
import provider.services.exceptions.SubscriptionException;

import java.sql.SQLException;
import java.util.Date;
import java.util.Optional;

public class SubscriptionServiceImpl implements SubscriptionService {

    private final SubscriptionDAO subscriptionDAO;
    private final TransactionService transactionService;

    public SubscriptionServiceImpl() {
        subscriptionDAO = new SubscriptionDAOImpl();
        transactionService = new TransactionServiceImpl();
    }

    @Override
    public void subscribeUserToTariff(User user, Tariff tariff) throws SQLException, SubscriptionException {

        if (user.getBalance() >= tariff.getPrice()) {
            final Transaction transaction = new Transaction(tariff.getPrice());
            transactionService.processNewWithdrawal(user, transaction);
            subscriptionDAO.updateEndDateByUserId(user.getId(), new Date());
            createNewSubscription(user, tariff);
        } else {
            throw new SubscriptionException("User does not have enough money");
        }
    }

    @Override
    public Optional<Subscription> findActiveSubscription(User user) throws SQLException {

        if (user != null) {
            int userId = user.getId();
            return subscriptionDAO.getCurrentSubscription(userId);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void deleteByTariffId(int id) throws SQLException {
        subscriptionDAO.deleteByTariffId(id);
    }

    private void createNewSubscription(User user, Tariff tariff) throws SQLException {
        final Subscription subscription = new Subscription(tariff.getId(), user.getId(), new Date());
        subscriptionDAO.save(subscription);
    }

}
