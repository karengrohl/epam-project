package provider.services;

import provider.domain.User;

import javax.servlet.http.HttpServletRequest;

public interface SessionService {
    User getCurrentUser(HttpServletRequest request);

    void setLocalization(HttpServletRequest request, String locale);
}
