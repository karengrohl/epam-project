package provider.services;

import provider.dao.UserDAO;
import provider.dao.UserDAOImpl;
import provider.domain.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {

    private final UserDAO userDAO;

    public UserServiceImpl() {
        userDAO = new UserDAOImpl();
    }

    @Override
    public void saveToDatabase(User user) throws SQLException {
        userDAO.save(user);
    }

    @Override
    public Optional<User> findByEmail(String email) throws SQLException {
        return userDAO.findByEmail(email);
    }

    @Override
    public void updatePersonalInformation(int id, User user) throws SQLException {
        userDAO.updatePersonalInformation(id, user);
    }

    @Override
    public void alterUserBalance(int userId, double amount) throws SQLException {
        final User existingUser = userDAO.findById(userId).get();
        final double newBalance = existingUser.getBalance() + amount;
        userDAO.updateBalance(userId, newBalance);
    }

    @Override
    public User findById(int id) throws SQLException {
        return userDAO.findById(id).get();
    }

    @Override
    public List<User> findAll() throws SQLException {
        return userDAO.findAll();
    }

    @Override
    public void blockUser(int userId) throws SQLException {
        userDAO.changeIsBlocked(userId, true);
    }

    @Override
    public void unblockUser(int userId) throws SQLException {
        userDAO.changeIsBlocked(userId, false);
    }

    @Override
    public void withdrawAmount(int userId, double amount) throws SQLException {
        final User user = userDAO.findById(userId).get();
        final double newBalance = user.getBalance() - amount;
        userDAO.updateBalance(userId, newBalance);
    }

    @Override
    public boolean checkLogin(String email, String password) throws SQLException {
        final Optional<User> optionalUser = userDAO.findByEmail(email);

        if (optionalUser.isPresent()) {
            final User user = optionalUser.get();
            return email.equals(user.getEmail()) && password.equals(user.getPassword()) && !user.getIsBlocked();
        } else {
            return false;
        }
    }
}
