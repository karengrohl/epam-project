package provider.services;

import provider.dao.TransactionDao;
import provider.dao.TransactionDaoImpl;
import provider.domain.Transaction;
import provider.domain.User;

import java.sql.SQLException;
import java.time.LocalDateTime;

public class TransactionServiceImpl implements TransactionService {

    private final TransactionDao transactionDao;
    private final UserService userService;

    public TransactionServiceImpl() {
        transactionDao = new TransactionDaoImpl();
        userService = new UserServiceImpl();
    }

    @Override
    public void processNewPayment(User user, Transaction transaction, boolean isPromised) throws SQLException {
        if (isPromised) {
            transaction.setType(Transaction.Type.PROMISED_PAYMENT);
        } else {
            transaction.setType(Transaction.Type.PAYMENT);
        }
        saveTransaction(user, transaction);
        userService.alterUserBalance(user.getId(), transaction.getAmount());
    }

    @Override
    public void processNewWithdrawal(User user, Transaction transaction) throws SQLException {
        transaction.setType(Transaction.Type.WITHDRAWAL);
        saveTransaction(user, transaction);
        userService.withdrawAmount(user.getId(), transaction.getAmount());
    }


    private void saveTransaction(User user, Transaction transaction) throws SQLException {
        transaction.setDate(LocalDateTime.now());
        transaction.setUserId(user.getId());
        transactionDao.save(transaction);
    }
}
