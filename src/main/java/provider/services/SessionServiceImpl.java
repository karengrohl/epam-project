package provider.services;

import provider.domain.User;

import javax.servlet.http.HttpServletRequest;

public class SessionServiceImpl implements SessionService {

    private static final String LOCALIZATION_KEY = "javax.servlet.jsp.jstl.fmt.locale.session";
    private static final String USER_KEY = "user";

    @Override
    public User getCurrentUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute(USER_KEY);
    }

    @Override
    public void setLocalization(HttpServletRequest request, String locale) {
        request.getSession().setAttribute(LOCALIZATION_KEY, locale);
    }
}
