package provider.services;

import provider.dao.TariffDAO;
import provider.dao.TariffDAOImpl;
import provider.domain.Tariff;

import java.sql.SQLException;
import java.util.Optional;

public class TariffServiceImpl implements TariffService {

    private final TariffDAO tariffDAO;

    public TariffServiceImpl() {
        tariffDAO = new TariffDAOImpl();
    }

    @Override
    public void saveToDatabase(Tariff tariff) throws SQLException {
        tariffDAO.save(tariff);
    }

    @Override
    public Optional<Tariff> findTariffById(int id) throws SQLException {
        return tariffDAO.findById(id);
    }

    @Override
    public void update(int id, Tariff tariff) throws SQLException {
        tariffDAO.update(id, tariff);
    }

    @Override
    public void deleteById(int id) throws SQLException {
        tariffDAO.deleteById(id);
    }
}
