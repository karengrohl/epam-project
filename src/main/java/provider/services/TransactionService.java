package provider.services;

import provider.domain.Transaction;
import provider.domain.User;

import java.sql.SQLException;

public interface TransactionService {
    void processNewPayment(User user, Transaction transaction, boolean isPromised) throws SQLException;

    void processNewWithdrawal(User user, Transaction transaction) throws SQLException;
}
