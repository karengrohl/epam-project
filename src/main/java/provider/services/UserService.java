package provider.services;

import provider.domain.Tariff;
import provider.domain.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface UserService {

    void saveToDatabase(User user) throws SQLException;

    Optional<User> findByEmail(String email) throws SQLException;

    void updatePersonalInformation(int id, User user) throws SQLException;

    void alterUserBalance(int userId, double amount) throws SQLException;

    User findById(int id) throws SQLException;

    List<User> findAll() throws SQLException;

    void blockUser(int userId) throws SQLException;

    void unblockUser(int userId) throws SQLException;

    void withdrawAmount(int userId, double price) throws SQLException;

    public boolean checkLogin(String email, String password) throws SQLException;
}
