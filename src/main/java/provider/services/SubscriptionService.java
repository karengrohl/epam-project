package provider.services;

import provider.domain.Subscription;
import provider.domain.Tariff;
import provider.domain.User;
import provider.services.exceptions.SubscriptionException;

import java.sql.SQLException;
import java.util.Optional;

public interface SubscriptionService {

    void subscribeUserToTariff(User user, Tariff tariff) throws SQLException, SubscriptionException;

    Optional<Subscription> findActiveSubscription(User user) throws SQLException;

    void deleteByTariffId(int id) throws SQLException;
}
