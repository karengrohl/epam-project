package provider.services;

import provider.domain.Tariff;

import java.sql.SQLException;
import java.util.Optional;

public interface TariffService {

    void saveToDatabase(Tariff tariff) throws SQLException;

    Optional<Tariff> findTariffById(int id) throws SQLException;

    void update(int id, Tariff tariff) throws SQLException;

    void deleteById(int id) throws SQLException;
}
